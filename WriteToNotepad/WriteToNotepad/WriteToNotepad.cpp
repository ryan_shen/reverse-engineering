﻿#include <iostream>
#include <stdio.h>
#include <Windows.h>
#include <string>
#include <conio.h>
#include <cstdlib>
#include <fstream>
#include <stdlib.h>
using namespace std;

int main()
{
    HWND hNotepad = FindWindow(L"Notepad", nullptr);
    if (hNotepad == nullptr) {
        system("start notepad");
        Sleep(1000);
        hNotepad = FindWindow(L"Notepad", nullptr);
    }
    HWND hEdit = FindWindowEx(hNotepad, nullptr, L"Edit", nullptr);

    while (1) {
        string input;
        cout << "Your input: ";
        getline(cin, input);
        for (int i = 0; i < input.length(); i++)
        {
            MessageBeep(MB_ICONINFORMATION);
            SendMessage(hEdit, WM_CHAR, input[i], 0);
            if (input[i] != ' ') {
                Sleep(200);
            }
        }
        SendMessage(hEdit, WM_CHAR, 10, 0);
    }
}
