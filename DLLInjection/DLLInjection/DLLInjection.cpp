﻿#include <iostream>
#include <Windows.h>
#include <tlhelp32.h>
using namespace std;

//取得 PID
DWORD GetProcessIdByName(LPCTSTR processName) {
    PROCESSENTRY32 pt;
    HANDLE hsnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    pt.dwSize = sizeof(PROCESSENTRY32);
    if (Process32First(hsnap, &pt)) { // must call this first
        do {
            if (!lstrcmpi(pt.szExeFile, processName)) {
                CloseHandle(hsnap);
                return pt.th32ProcessID;
            }
        } while (Process32Next(hsnap, &pt));
    }
    CloseHandle(hsnap); // close handle on failure
    return 0;
}

int main()
{
#if _WIN64
    LPCSTR DllPath = "C:\\Users\\Ryan\\Desktop\\VSProject\\DLLInjection\\x64\\Debug\\TestDLL.dll";
#else
    LPCSTR DllPath = "C:\\Users\\Ryan\\Desktop\\VSProject\\DLLInjection\\Debug\\TestDLL.dll";
#endif // _WIN64

    HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, GetProcessIdByName(L"Notepad.exe"));

    //動態配置記憶體空間
    LPVOID lpDllStartAddress = VirtualAllocEx(hProcess, 0, strlen(DllPath) + 1,
                                     MEM_COMMIT, PAGE_READWRITE);

    //寫入 DLL 字串
    WriteProcessMemory(hProcess, lpDllStartAddress, (LPVOID)DllPath, strlen(DllPath) + 1, 0);

    //取得此程式裡面 LoadLibrary 的所在位址 (因為 kernel32.dll 在虛擬記憶體裡面的位址都一樣)
    LPTHREAD_START_ROUTINE lpLoadLibraryStartAddress = (LPTHREAD_START_ROUTINE)GetProcAddress(GetModuleHandleA("Kernel32.dll"), "LoadLibraryA");
    HANDLE hLoadThread = CreateRemoteThread(hProcess, 0, 0, lpLoadLibraryStartAddress, lpDllStartAddress, 0, 0);

    //等待執行緒結束
    //WaitForSingleObject(hLoadThread, INFINITE);

    //cout << "Dll path allocated at: " << std::hex << lpDllStartAddress << std::endl;
    //cin.get();

    ////釋放記憶體
    //VirtualFreeEx(hProcess, lpDllStartAddress, strlen(DllPath) + 1, MEM_RELEASE);

    return 0;
}