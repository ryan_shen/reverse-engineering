﻿
namespace GurasAdventureCheat
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.Health = new System.Windows.Forms.Label();
            this.GameTimer = new System.Windows.Forms.Timer(this.components);
            this.LockHealth = new System.Windows.Forms.CheckBox();
            this.BossHealth = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Score = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ScoreTextBox = new System.Windows.Forms.TextBox();
            this.ScoreButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.label1.Location = new System.Drawing.Point(69, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "當前血量";
            // 
            // Health
            // 
            this.Health.AutoSize = true;
            this.Health.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.Health.Location = new System.Drawing.Point(229, 71);
            this.Health.Name = "Health";
            this.Health.Size = new System.Drawing.Size(25, 27);
            this.Health.TabIndex = 1;
            this.Health.Text = "0";
            // 
            // GameTimer
            // 
            this.GameTimer.Enabled = true;
            this.GameTimer.Interval = 30;
            this.GameTimer.Tick += new System.EventHandler(this.GameTimer_Tick);
            // 
            // LockHealth
            // 
            this.LockHealth.AutoSize = true;
            this.LockHealth.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.LockHealth.Location = new System.Drawing.Point(395, 67);
            this.LockHealth.Name = "LockHealth";
            this.LockHealth.Size = new System.Drawing.Size(152, 31);
            this.LockHealth.TabIndex = 2;
            this.LockHealth.Text = "滿血鎖定";
            this.LockHealth.UseVisualStyleBackColor = true;
            // 
            // BossHealth
            // 
            this.BossHealth.AutoSize = true;
            this.BossHealth.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.BossHealth.Location = new System.Drawing.Point(229, 203);
            this.BossHealth.Name = "BossHealth";
            this.BossHealth.Size = new System.Drawing.Size(25, 27);
            this.BossHealth.TabIndex = 7;
            this.BossHealth.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.label5.Location = new System.Drawing.Point(69, 203);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 27);
            this.label5.TabIndex = 6;
            this.label5.Text = "魔王血量";
            // 
            // Score
            // 
            this.Score.AutoSize = true;
            this.Score.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.Score.Location = new System.Drawing.Point(229, 138);
            this.Score.Name = "Score";
            this.Score.Size = new System.Drawing.Size(25, 27);
            this.Score.TabIndex = 11;
            this.Score.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.label4.Location = new System.Drawing.Point(69, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 27);
            this.label4.TabIndex = 10;
            this.label4.Text = "分數";
            // 
            // ScoreTextBox
            // 
            this.ScoreTextBox.Location = new System.Drawing.Point(395, 128);
            this.ScoreTextBox.Name = "ScoreTextBox";
            this.ScoreTextBox.Size = new System.Drawing.Size(152, 36);
            this.ScoreTextBox.TabIndex = 12;
            this.ScoreTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ScoreTextBox_KeyPress);
            // 
            // ScoreButton
            // 
            this.ScoreButton.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.ScoreButton.Location = new System.Drawing.Point(566, 128);
            this.ScoreButton.Name = "ScoreButton";
            this.ScoreButton.Size = new System.Drawing.Size(96, 45);
            this.ScoreButton.TabIndex = 13;
            this.ScoreButton.Text = "變更";
            this.ScoreButton.UseVisualStyleBackColor = true;
            this.ScoreButton.Click += new System.EventHandler(this.ScoreButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.label2.Location = new System.Drawing.Point(395, 205);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(346, 27);
            this.label2.TabIndex = 14;
            this.label2.Text = "透過 [S] 鍵將魔王血量降至 1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ScoreButton);
            this.Controls.Add(this.ScoreTextBox);
            this.Controls.Add(this.Score);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BossHealth);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LockHealth);
            this.Controls.Add(this.Health);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "GurasAdventureCheat";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Health;
        private System.Windows.Forms.Timer GameTimer;
        private System.Windows.Forms.CheckBox LockHealth;
        private System.Windows.Forms.Label BossHealth;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Score;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ScoreTextBox;
        private System.Windows.Forms.Button ScoreButton;
        private System.Windows.Forms.Label label2;
    }
}

