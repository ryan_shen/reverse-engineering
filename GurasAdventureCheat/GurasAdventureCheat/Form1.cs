﻿using GameCheat;
using System;
using System.Windows.Forms;
using static GameCheat.CheatEnum;

namespace GurasAdventureCheat
{
    public partial class Form1 : Form
    {
        private readonly string _gameName = "Gura's Adventure";
        private readonly CheatProcess _process;
        //程式的進入點
        private readonly int _entryAddress = 0x00400000;
        //修改器上需要用到的位址
        private int _playerAddress;
        private int _playerHealthAddress;
        private int _playerScoreAddress;
        private int _bossHealthAddress;

        public Form1()
        {
            InitializeComponent();

            _process = new CheatProcess(_gameName);
            _process.RegisterHotKey(this, WindowsConstant.NO_MOD, Keys.S);
        }

        private void GameTimer_Tick(object sender, EventArgs e)
        {
            switch (_process.State)
            {
                case ProcessState.Running:
                    OnRunning();
                    break;
                case ProcessState.Closed:
                    break;
            }
        }

        /// <summary>
        /// 由 Windows 傳來的事件
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            //當事件名稱為觸發熱鍵時
            if(m.Msg == WindowsConstant.WM_HOTKEY)
            {
                //WParam 所帶的值為觸發的熱鍵
                if ((int)m.WParam == (int)(WindowsConstant.NO_MOD | Keys.S))
                {
                    _process.WriteMemory(_bossHealthAddress, 1.0);
                }
            }
            
            base.WndProc(ref m);
        }

        /// <summary>
        /// 遊戲執行中不斷執行的事件
        /// </summary>
        private void OnRunning()
        {
            _playerAddress = _process.ReadMemory<int>(_entryAddress + 0x6FCAF4, + 0x30);
            _playerHealthAddress = _process.ReadMemory<int>(_playerAddress + 0xAA4);
            _playerScoreAddress = _process.ReadMemory<int>(_playerAddress + 0xD8C);
            _bossHealthAddress = _process.ReadMemory<int>(_entryAddress + 0x6F1F6C, 0, 0x14C, 0x14C, 0x2C, 0x10, 0xE4);

            //鎖定血量
            CheckLockHealth();
            //顯示當前血量
            Health.Text = _process.ReadMemory<double>(_playerHealthAddress).ToString();
            //顯示當前分數
            Score.Text = _process.ReadMemory<double>(_playerScoreAddress).ToString();
            //顯示魔王血量
            BossHealth.Text = _process.ReadMemory<double>(_bossHealthAddress).ToString();
        }

        #region 鎖定血量
        /// <summary>
        /// 鎖定血量
        /// </summary>
        private void CheckLockHealth()
        {
            if (LockHealth.Checked)
            {
                _process.WriteMemory(_playerHealthAddress, 100.0);
            }
        }
        #endregion

        private void ScoreTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void ScoreButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ScoreTextBox.Text))
            {
                return;
            }

            _process.WriteMemory(_playerScoreAddress, Convert.ToDouble(ScoreTextBox.Text));
            ScoreTextBox.Clear();
        }
    }
}
