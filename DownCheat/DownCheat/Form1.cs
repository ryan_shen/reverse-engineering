﻿using Cheat;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using static Cheat.CheatEnum;

namespace DownCheat
{
    public partial class Form1 : Form
    {
        private readonly string _gameName = "down";
        private readonly string _windowTitle = "NS-SHAFT";
        private readonly CheatProcess _process;
        //程式的進入點
        private readonly int _entryAddress = 0x00400000;
        //THREADSTACK0
        private readonly int _baseAddress = 0x0019FF7C - 0x0000026C;
        //人物基址
        private int _playerAddress;

        public Form1()
        {
            InitializeComponent();

            _process = new CheatProcess(_gameName);
        }

        /// <summary>
        /// 遊戲執行中不斷執行的事件
        /// </summary>
        private void OnRunning()
        {
            _playerAddress = _process.ReadMemory(_baseAddress);

            //因為 THREADSTACK0 指向的位址在開始畫面會一直變，所以要做檢核不然會寫到錯誤的位址導致 Crash
            var blood = _process.ReadMemory(_playerAddress + 0x1170);
            if (blood <= 0 || blood > 12)
            {
                return;
            }

            //鎖定血量
            ToggleLockHealth();
            //顯示當前血量
            Health.Text = _process.ReadMemory(_playerAddress + 0x1170).ToString();
            //顯示游標與玩家座標
            ShowCoordinate();
            //顯示當前階層
            Layer.Text = _process.ReadMemory(_playerAddress + 0x1300).ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            switch (_process.State)
            {
                case ProcessState.Running:
                    OnRunning();
                    break;
                case ProcessState.Closed:
                    break;
            }
        }

        #region 增加層數
        private void Add10Layers_Click(object sender, EventArgs e)
        {
            AddLayers(10);
        }

        private void Add100Layers_Click(object sender, EventArgs e)
        {
            AddLayers(100);
        }

        private void Add1000Layers_Click(object sender, EventArgs e)
        {
            AddLayers(1000);
        }

        /// <summary>
        /// 增加層數
        /// </summary>
        /// <param name="layerCount">要增加的層的數量</param>
        private void AddLayers(int layerCount)
        {
            var currentLayer = _process.ReadMemory(_playerAddress + 0x1300);
            _process.WriteMemory(_playerAddress + 0x1300, currentLayer + layerCount);
        }
        #endregion

        #region 鎖定血量
        /// <summary>
        /// 鎖定血量
        /// </summary>
        private void ToggleLockHealth()
        {
            if (LockHealth.Checked)
            {
                _process.FillNOP(_entryAddress + 0x6311, 8);
                _process.FillNOP(_entryAddress + 0x661E, 8);
                _process.FillNOP(_entryAddress + 0x6991, 8);
                _process.FillNOP(_entryAddress + 0x6A3B, 7);
            }
            else
            {
                //天花板的刺扣血
                _process.WriteMemory(_entryAddress + 0x6311, new byte[] { 0x83, 0xAC, 0x08, 0x70, 0x11, 0x00, 0x00, 0x05 });
                _process.WriteMemory(_entryAddress + 0x661E, new byte[] { 0x83, 0xAC, 0x08, 0x70, 0x11, 0x00, 0x00, 0x05 });
                //踩刺扣血
                _process.WriteMemory(_entryAddress + 0x6991, new byte[] { 0x83, 0xAC, 0x08, 0x70, 0x11, 0x00, 0x00, 0x06 });
                //踩地板加血
                _process.WriteMemory(_entryAddress + 0x6A3B, new byte[] { 0xFF, 0x84, 0x08, 0x70, 0x11, 0x00, 0x00 });
            }
        }
        #endregion

        private void ShowCoordinate()
        {
            Point cursorPosition = _process.GetCursorPosition();
            int cursorX = cursorPosition.X;
            int cursorY = cursorPosition.Y;
            CursorX.Text = cursorX.ToString();
            CursorY.Text = cursorY.ToString();

            //玩家原始座標
            int playerX = _process.ReadMemory(_playerAddress + 0x1158);
            int playerY = _process.ReadMemory(_playerAddress + 0x115C);

            if (ControlWithCursor.Checked)
            {
                //計算玩家新座標
                var window = _process.GetWindowRectangle(_windowTitle);
                playerX = cursorX - window.Left - 60;
                if (playerX < 0)
                {
                    playerX = 0;
                }
                else if (playerX > 352)
                {
                    playerX = 352;
                }

                playerY = cursorY - window.Top - 120;
                if (playerY < 0)
                {
                    playerY = 0;
                }
                else if (playerY > 359)
                {
                    //一到 359 就算輸
                    playerY = 359;
                }

                _process.WriteMemory(_playerAddress + 0x1158, playerX);
                _process.WriteMemory(_playerAddress + 0x115C, playerY);
                //移除重力
                _process.WriteMemory(_playerAddress + 0x1164, 0);
            }

            PlayerX.Text = playerX.ToString();
            PlayerY.Text = playerY.ToString();
        }
    }
}
