﻿namespace DownCheat
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.Health = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.LockHealth = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CursorX = new System.Windows.Forms.Label();
            this.CursorY = new System.Windows.Forms.Label();
            this.PlayerY = new System.Windows.Forms.Label();
            this.PlayerX = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ControlWithCursor = new System.Windows.Forms.CheckBox();
            this.Layer = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Add10Layers = new System.Windows.Forms.Button();
            this.Add1000Layers = new System.Windows.Forms.Button();
            this.Add100Layers = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 10F);
            this.label1.Location = new System.Drawing.Point(69, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "當前血量";
            // 
            // Health
            // 
            this.Health.AutoSize = true;
            this.Health.Font = new System.Drawing.Font("新細明體", 10F);
            this.Health.Location = new System.Drawing.Point(229, 71);
            this.Health.Name = "Health";
            this.Health.Size = new System.Drawing.Size(25, 27);
            this.Health.TabIndex = 1;
            this.Health.Text = "0";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 30;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // LockHealth
            // 
            this.LockHealth.AutoSize = true;
            this.LockHealth.Font = new System.Drawing.Font("新細明體", 10F);
            this.LockHealth.Location = new System.Drawing.Point(395, 67);
            this.LockHealth.Name = "LockHealth";
            this.LockHealth.Size = new System.Drawing.Size(98, 31);
            this.LockHealth.TabIndex = 2;
            this.LockHealth.Text = "鎖定";
            this.LockHealth.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 10F);
            this.label2.Location = new System.Drawing.Point(69, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 27);
            this.label2.TabIndex = 3;
            this.label2.Text = "滑鼠座標";
            // 
            // CursorX
            // 
            this.CursorX.AutoSize = true;
            this.CursorX.Font = new System.Drawing.Font("新細明體", 10F);
            this.CursorX.Location = new System.Drawing.Point(229, 138);
            this.CursorX.Name = "CursorX";
            this.CursorX.Size = new System.Drawing.Size(25, 27);
            this.CursorX.TabIndex = 4;
            this.CursorX.Text = "0";
            // 
            // CursorY
            // 
            this.CursorY.AutoSize = true;
            this.CursorY.Font = new System.Drawing.Font("新細明體", 10F);
            this.CursorY.Location = new System.Drawing.Point(307, 138);
            this.CursorY.Name = "CursorY";
            this.CursorY.Size = new System.Drawing.Size(25, 27);
            this.CursorY.TabIndex = 5;
            this.CursorY.Text = "0";
            // 
            // PlayerY
            // 
            this.PlayerY.AutoSize = true;
            this.PlayerY.Font = new System.Drawing.Font("新細明體", 10F);
            this.PlayerY.Location = new System.Drawing.Point(307, 203);
            this.PlayerY.Name = "PlayerY";
            this.PlayerY.Size = new System.Drawing.Size(25, 27);
            this.PlayerY.TabIndex = 8;
            this.PlayerY.Text = "0";
            // 
            // PlayerX
            // 
            this.PlayerX.AutoSize = true;
            this.PlayerX.Font = new System.Drawing.Font("新細明體", 10F);
            this.PlayerX.Location = new System.Drawing.Point(229, 203);
            this.PlayerX.Name = "PlayerX";
            this.PlayerX.Size = new System.Drawing.Size(25, 27);
            this.PlayerX.TabIndex = 7;
            this.PlayerX.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("新細明體", 10F);
            this.label5.Location = new System.Drawing.Point(69, 203);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 27);
            this.label5.TabIndex = 6;
            this.label5.Text = "人物座標";
            // 
            // ControlWithCursor
            // 
            this.ControlWithCursor.AutoSize = true;
            this.ControlWithCursor.Font = new System.Drawing.Font("新細明體", 10F);
            this.ControlWithCursor.Location = new System.Drawing.Point(395, 199);
            this.ControlWithCursor.Name = "ControlWithCursor";
            this.ControlWithCursor.Size = new System.Drawing.Size(152, 31);
            this.ControlWithCursor.TabIndex = 9;
            this.ControlWithCursor.Text = "用滑鼠玩";
            this.ControlWithCursor.UseVisualStyleBackColor = true;
            // 
            // Layer
            // 
            this.Layer.AutoSize = true;
            this.Layer.Font = new System.Drawing.Font("新細明體", 10F);
            this.Layer.Location = new System.Drawing.Point(229, 271);
            this.Layer.Name = "Layer";
            this.Layer.Size = new System.Drawing.Size(25, 27);
            this.Layer.TabIndex = 11;
            this.Layer.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 10F);
            this.label4.Location = new System.Drawing.Point(69, 271);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 27);
            this.label4.TabIndex = 10;
            this.label4.Text = "當前階層";
            // 
            // Add10Layers
            // 
            this.Add10Layers.Font = new System.Drawing.Font("新細明體", 10F);
            this.Add10Layers.Location = new System.Drawing.Point(323, 263);
            this.Add10Layers.Name = "Add10Layers";
            this.Add10Layers.Size = new System.Drawing.Size(96, 42);
            this.Add10Layers.TabIndex = 12;
            this.Add10Layers.Text = "+10";
            this.Add10Layers.UseVisualStyleBackColor = true;
            this.Add10Layers.Click += new System.EventHandler(this.Add10Layers_Click);
            // 
            // Add1000Layers
            // 
            this.Add1000Layers.AutoSize = true;
            this.Add1000Layers.Font = new System.Drawing.Font("新細明體", 10F);
            this.Add1000Layers.Location = new System.Drawing.Point(599, 263);
            this.Add1000Layers.Name = "Add1000Layers";
            this.Add1000Layers.Size = new System.Drawing.Size(96, 42);
            this.Add1000Layers.TabIndex = 13;
            this.Add1000Layers.Text = "+1000";
            this.Add1000Layers.UseVisualStyleBackColor = true;
            this.Add1000Layers.Click += new System.EventHandler(this.Add1000Layers_Click);
            // 
            // Add100Layers
            // 
            this.Add100Layers.Font = new System.Drawing.Font("新細明體", 10F);
            this.Add100Layers.Location = new System.Drawing.Point(461, 263);
            this.Add100Layers.Name = "Add100Layers";
            this.Add100Layers.Size = new System.Drawing.Size(96, 42);
            this.Add100Layers.TabIndex = 14;
            this.Add100Layers.Text = "+100";
            this.Add100Layers.UseVisualStyleBackColor = true;
            this.Add100Layers.Click += new System.EventHandler(this.Add100Layers_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Add100Layers);
            this.Controls.Add(this.Add1000Layers);
            this.Controls.Add(this.Add10Layers);
            this.Controls.Add(this.Layer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ControlWithCursor);
            this.Controls.Add(this.PlayerY);
            this.Controls.Add(this.PlayerX);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CursorY);
            this.Controls.Add(this.CursorX);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LockHealth);
            this.Controls.Add(this.Health);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "DownCheat";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Health;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox LockHealth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label CursorX;
        private System.Windows.Forms.Label CursorY;
        private System.Windows.Forms.Label PlayerY;
        private System.Windows.Forms.Label PlayerX;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox ControlWithCursor;
        private System.Windows.Forms.Label Layer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Add10Layers;
        private System.Windows.Forms.Button Add1000Layers;
        private System.Windows.Forms.Button Add100Layers;
    }
}

