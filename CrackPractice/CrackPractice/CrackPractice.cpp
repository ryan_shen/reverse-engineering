#include <iostream>
#include <stdio.h>
#include <Windows.h>
#include <string>
#include <conio.h>
#include <cstdlib>
#include <fstream>
#include <stdlib.h>
using namespace std;

#define MENU_BEEP 101
#define IDC_REGISTER 102

HINSTANCE hInst;                                // 目前執行個體
HWND hUserName;     //使用者名稱
HWND hProductKey;   //產品金鑰
HWND hRegister;     //註冊字串
bool bRegistered = false;

ATOM                MyRegisterClass();
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
void doNothing(int);
void AddMenu(HWND);
void AddControl(HWND);

/// <summary>
/// 主程式進入點
/// </summary>
/// <param name="hInstance">當前 EXE 在記憶體中的起始位址</param>
/// <param name="hPrevInstance"></param>
/// <param name="lpCmdLine">命令列參數</param>
/// <param name="nCmdShow"></param>
/// <returns></returns>
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                      _In_opt_ HINSTANCE hPrevInstance,
                      _In_ LPWSTR    lpCmdLine,
                      _In_ int       nCmdShow)
{
    hInst = hInstance; // 將執行個體控制代碼儲存在全域變數中

    //註冊主視窗
    MyRegisterClass();

    //建立主視窗 (WS_OVERLAPPEDWINDOW 代表要有放大縮小關閉按鈕)
    HWND hWnd = CreateWindowW(L"Main Window", L"CrackPractice", WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                              300, 300, 400, 230, nullptr, nullptr, hInst, nullptr);
    if (!hWnd)
    {
        return FALSE;
    }

    //必須創一個無窮迴圈等待訊息，不然程式會直接結束
    MSG msg;
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        //轉換訊息，因為鍵盤按下和彈起會發送 WM_KEYDOWN 和 WM_KEYUP，此函式負責將他轉成實際輸入的 WM_CHAR
        TranslateMessage(&msg);
        //把訊息傳送到 WndProc()
        DispatchMessage(&msg);
    }

    return (int)msg.wParam;
}

/// <summary>
/// 註冊主視窗
/// </summary>
/// <param name="hInstance"></param>
/// <returns></returns>
ATOM MyRegisterClass()
{
    //初始值都設 0，因為如果有其中一個屬性沒有初始化視窗就建立不起來
    WNDCLASSW wcex = { 0 };

    //視窗樣式 (水平垂直調整視窗時會觸發 WM_PAINT 事件，但好像不設定也會觸發)
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    //處理視窗訊息的函式
    wcex.lpfnWndProc = WndProc;
    //視窗實例
    wcex.hInstance = hInst;
    //視窗 Icon
    wcex.hIcon = nullptr;
    //游標圖案
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    //背景顏色
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW);
    //Menu 名稱
    wcex.lpszMenuName = nullptr;
    //視窗類別名稱 (不能與系統已定義的類別名稱重複，如 button)
    wcex.lpszClassName = L"Main Window";

    return RegisterClassW(&wcex);
}

/// <summary>
/// 接收訊息
/// </summary>
/// <param name="hWnd">視窗控制代碼</param>
/// <param name="message">訊息種類</param>
/// <param name="wParam">訊息資料內容</param>
/// <param name="lParam">訊息指標</param>
/// <returns></returns>
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
        case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // 剖析功能表選取項目:
            switch (wmId)
            {
                case 1:
                {
                    char c[100];
                    GetWindowTextA(hProductKey, c, 100);
                    string s(c);

                    //SetWindowText(hWnd, w);
                    if (s.length() != 4) {
                        MessageBoxA(hWnd, "Invalid length of the key.", "Error", MB_OK);
                        break;
                    }

                    bool hasInvalidCharacter = false;
                    for (int i = 0; i < s.length(); i++) {
                        if (c[i] >= 48 && c[i] <= 57 || c[i] >= 97 && c[i] <= 102) {
                            continue;
                        }
                        hasInvalidCharacter = true;
                        break;
                    }
                    if (hasInvalidCharacter) {
                        MessageBoxA(hWnd, "Invalid character of the key.", "Error", MB_OK);
                        break;
                    }

                    int value = stoi(s, 0, 16);
                    if (value % 8 != 7) {
                        MessageBoxA(hWnd, "Invalid key.", "Error", MB_OK);
                        break;
                    }

                    doNothing(5);
                    GetWindowTextA(hUserName, c, 100);
                    string userName(c);
                    userName.append(", thank you for your purchasing.");
                    MessageBoxA(hWnd, userName.c_str(), "Success", MB_OK);
                    bRegistered = true;
                    SetWindowText(hRegister, L"Registered");
                    break;
                }
                case MENU_BEEP:
                {
                    MessageBeep(MB_OK);
                }
                default:
                    return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
        //縮小後再放大時，或是調整視窗大小時呼叫 (從其他視窗後面浮出不會呼叫)
        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: 在此新增任何使用 hdc 的繪圖程式碼...
            EndPaint(hWnd, &ps);
        }
        break;
        //主視窗第一次建立時呼叫
        case WM_CREATE:
        {
            AddMenu(hWnd);
            AddControl(hWnd);
        }
        break;
        //按下 [x] 時呼叫
        case WM_DESTROY:
            //離開訊息迴圈，並回傳 ExitCode
            PostQuitMessage(0);
            break;
        case WM_CTLCOLORSTATIC:
        {
            //表示「已註冊/未註冊」的字串
            int id = GetDlgCtrlID((HWND)lParam);
            HDC hdlControl = (HDC)wParam;
            if (id == IDC_REGISTER) {
                bRegistered ?
                    SetTextColor(hdlControl, RGB(0, 255, 0)) :
                    SetTextColor(hdlControl, RGB(255, 0, 0));
            }
            SetBkColor(hdlControl, GetSysColor(COLOR_MENU));
            return (INT_PTR)GetSysColorBrush(COLOR_MENU);
        }
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

/// <summary>
/// 什麼都不做
/// </summary>
/// <param name="i"></param>
void doNothing(int n = 3) {
    string s = "This function will not help you crack this shit.";
}

/// <summary>
/// 添加 Menu
/// </summary>
/// <param name="hWnd">主視窗</param>
void AddMenu(HWND hWnd) {
    //主要的 Menu 列
    HMENU hMenu = CreateMenu();

    //插入項目，MENU_ID 表示點擊該項目的時候會將該值存到 wParam 後傳到 WndProc() 內
    AppendMenu(hMenu, MF_STRING, MENU_BEEP, L"Beep");

    //用來展開子 Menu，當設定為 MF_POPUP 時 ID 應該要設為當前被展開的 Menu 的控制代碼
    HMENU hAboutPopUpMenu = CreateMenu();
    AppendMenu(hMenu, MF_POPUP, (UINT_PTR)hAboutPopUpMenu, L"About");

    AppendMenu(hAboutPopUpMenu, MF_STRING, MENU_BEEP, L"Beep");

    HMENU hAuthorMenu = CreateMenu();
    AppendMenu(hAboutPopUpMenu, MF_POPUP, (UINT_PTR)hAuthorMenu, L"Author");
    AppendMenu(hAuthorMenu, MF_STRING, NULL, L"Ryan Shen");

    SetMenu(hWnd, hMenu);
}

/// <summary>
/// 初始化控制項
/// </summary>
/// <param name="hWnd">主視窗</param>
void AddControl(HWND hWnd) {
    //WS_CHILD 代表要黏著父視窗
    hRegister = CreateWindow(L"static", L"Unregistered", WS_VISIBLE | WS_CHILD, 280, 10, 100, 20, hWnd, (HMENU)IDC_REGISTER, NULL, NULL);
    CreateWindow(L"static", L"Your Name", WS_VISIBLE | WS_CHILD, 20, 10, 100, 20, hWnd, NULL, NULL, NULL);
    hUserName = CreateWindow(L"edit", L"", WS_VISIBLE | WS_BORDER | WS_CHILD, 20, 30, 350, 20, hWnd, NULL, NULL, NULL);
    CreateWindow(L"static", L"Product Key", WS_VISIBLE | WS_CHILD, 20, 60, 350, 20, hWnd, NULL, NULL, NULL);
    hProductKey = CreateWindow(L"edit", L"", WS_VISIBLE | WS_BORDER | WS_CHILD, 20, 80, 350, 20, hWnd, NULL, NULL, NULL);
    CreateWindow(L"button", L"Activate", WS_VISIBLE | WS_BORDER | WS_CHILD, 300, 120, 70, 30, hWnd, (HMENU)1, NULL, NULL);
}