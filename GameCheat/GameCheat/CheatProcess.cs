﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using static GameCheat.CheatEnum;

namespace GameCheat
{
    public class CheatProcess
    {
        //工作管理員看到的 PID
        private int _processId => GetProcessId(_processName);
        //工作管理員看到的名稱 (不含副檔名)
        private readonly string _processName;

        private ProcessState _state;
        public ProcessState State
        {
            get
            {
                if (GetProcessId(_processName) == 0)
                {
                    _state = ProcessState.Closed;
                }
                else
                {
                    _state = ProcessState.Running;
                }

                return _state;
            }
            set
            {
                _state = value;
            }
        }

        public CheatProcess(string processName)
        {
            _processName = processName;
        }

        /// <summary>
        /// 取得程式的 Process Id (PID)
        /// </summary>
        /// <param name="processName">程式名稱 (不需副檔名)</param>
        /// <returns></returns>
        public int GetProcessId(string processName)
        {
            Process[] processes = Process.GetProcessesByName(processName);

            return processes.Any() ? processes[0].Id : 0;
        }

        /// <summary>
        /// 從記憶體取出一個指定型態的值
        /// </summary>
        /// <param name="baseAddress">記憶體位址</param>
        /// <returns></returns>
        [HandleProcessCorruptedStateExceptions] //用來允許捕捉非托管代碼中的異常，目前測試只在 .NET Framework 有效，.NET 5.0 捕捉不到
        public T ReadMemory<T>(int baseAddress) where T : struct
        {
            try
            {
                byte[] buffer = new byte[Unsafe.SizeOf<T>()];
                //取得緩衝區位址
                IntPtr bufferAddress = Marshal.UnsafeAddrOfPinnedArrayElement(buffer, 0);
                //以最高權限取得程序的控制代碼，0x1FFFFF 等同 Windows.h 的 PROCESS_ALL_ACCESS
                IntPtr hProcess = WindowsApi.OpenProcess(0x1FFFFF, false, _processId);
                //將值讀入緩衝區
                WindowsApi.ReadProcessMemory(hProcess, (IntPtr)baseAddress, bufferAddress, buffer.Length, IntPtr.Zero);
                //關閉操作
                WindowsApi.CloseHandle(hProcess);

                //依照傳進來的 T 解析要取出的值的型別
                object value = null;
                switch (Type.GetTypeCode(typeof(T)))
                {
                    case TypeCode.Int32:
                        //從非托管內存中讀取一個 32 位帶符號整數
                        value = Marshal.ReadInt32(bufferAddress);
                        break;
                    case TypeCode.Single:
                        byte[] floatBuffer = new byte[4];
                        Marshal.Copy(bufferAddress, floatBuffer, 0, floatBuffer.Length);
                        //從非托管內存中讀取一個 64 位元 double 值
                        value = BitConverter.ToSingle(floatBuffer, 0);
                        break;
                    case TypeCode.Double:
                        byte[] doubleBuffer = new byte[8];
                        Marshal.Copy(bufferAddress, doubleBuffer, 0, doubleBuffer.Length);
                        //從非托管內存中讀取一個 64 位元 double 值
                        value = BitConverter.ToDouble(doubleBuffer, 0);
                        break;
                    default:
                        throw new Exception("Unknown type");
                }
                
                return (T)Convert.ChangeType(value, typeof(T));
            }
            //有時候基底位址指標所指向的位址會有一瞬間亂跳，因此需要吃掉異常防止 Crash
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return default;
            }
        }

        /// <summary>
        /// 從記憶體取出一個指定型態的值
        /// </summary>
        /// <example>
        /// 假設記憶體結構如下，要從基底位址 0x00 經過多個指標最後取得 12.0 的值
        /// -----------------------------------------------
        /// | 所存放的值 | 0x06 |      | 0x0C |      | 12.0 |
        /// |----------------------------------------------
        /// | 記憶體位址 | 0x00 | 0x06 | 0x08 | 0x0C | 0x10 |
        /// -----------------------------------------------
        /// 則呼叫方式為 ReadMemory＜double＞(0x00, 0x02, 0x04)
        /// </example>
        /// <param name="baseAddress">記憶體位址</param>
        /// <param name="offsets">每一次從記憶體取出位址後，要加上的偏移量。當記憶體位址為指標時才需要傳此值</param>
        /// <returns></returns>
        [HandleProcessCorruptedStateExceptions]
        public T ReadMemory<T>(int baseAddress, params int[] offsets) where T : struct
        {
            int address = ReadMemory<int>(baseAddress);

            for (int i = 0; i < offsets.Length; i++)
            {
                if (i == offsets.Length - 1)
                {
                    return ReadMemory<T>(address + offsets[i]);
                }

                address = ReadMemory<int>(address + offsets[i]);
            }

            throw new Exception("Offsets must be given");
        }

        /// <summary>
        /// 將值寫入記憶體
        /// </summary>
        /// <param name="baseAddress">記憶體位址</param>
        /// <param name="value">要寫入的值</param>
        [HandleProcessCorruptedStateExceptions]
        public void WriteMemory<T>(int baseAddress, T value)
        {
            //依照傳進來的 T 解析要寫入的值的型別
            switch (Type.GetTypeCode(typeof(T)))
            {
                case TypeCode.Int32:
                    WriteMemory(baseAddress, BitConverter.GetBytes((int)Convert.ChangeType(value, typeof(int))));
                    return;
                case TypeCode.Single:
                    WriteMemory(baseAddress, BitConverter.GetBytes((float)Convert.ChangeType(value, typeof(float))));
                    return;
                case TypeCode.Double:
                    WriteMemory(baseAddress, BitConverter.GetBytes((double)Convert.ChangeType(value, typeof(double))));
                    return;
                case TypeCode.String:
                    WriteMemory(baseAddress, Encoding.UTF8.GetBytes((string)Convert.ChangeType(value, typeof(string))));
                    return;
                default:
                    throw new Exception("Unknown type");
            }
        }

        /// <summary>
        /// 將值寫入記憶體
        /// </summary>
        /// <param name="baseAddress">記憶體位址</param>
        /// <param name="bytes">要寫入的位元組序列</param>
        [HandleProcessCorruptedStateExceptions]
        public void WriteMemory(int baseAddress, byte[] bytes)
        {
            try
            {
                //以最高權限取得程序的控制代碼，0x1FFFFF 等同 Windows.h 的 PROCESS_ALL_ACCESS
                IntPtr hProcess = WindowsApi.OpenProcess(0x1FFFFF, false, _processId);
                //寫入資料到指定的記憶體位址
                WindowsApi.WriteProcessMemory(hProcess, (IntPtr)baseAddress, bytes, bytes.Length, IntPtr.Zero);
                //關閉操作
                WindowsApi.CloseHandle(hProcess);
            }
            catch { }
        }

        /// <summary>
        /// 填充 NOP
        /// </summary>
        /// <param name="baseAddress">寫入起始位址</param>
        /// <param name="count">NOP 數量</param>
        public void FillNOP(int baseAddress, int count)
        {
            WriteMemory(baseAddress, Enumerable.Repeat<byte>(0x90, count).ToArray());
        }

        /// <summary>
        /// 取得游標當前位置
        /// </summary>
        /// <returns></returns>
        public Point GetCursorPosition()
        {
            WindowsApi.GetCursorPos(out Point lpPoint);

            return lpPoint;
        }

        /// <summary>
        /// 取得視窗所在的矩形區塊 (解析度 100%)
        /// </summary>
        /// <param name="windowTitle">視窗標題</param>
        /// <param name="windowClassName">視窗類別名稱 (可透過 Spy++ 查看)，若為 Null 則只比對標題名稱</param>
        /// <returns></returns>
        public Rectangle GetWindowRectangle(string windowTitle, string windowClassName = null)
        {
            var window = WindowsApi.FindWindow(windowClassName, windowTitle);
            WindowsApi.GetWindowRect(window, out Rectangle rectangle);

            return rectangle;
        }

        /// <summary>
        /// 註冊熱鍵，註冊後每當熱鍵觸發 Windows 就會傳送事件訊息給 Form 視窗
        /// </summary>
        /// <param name="form">修改器本身</param>
        /// <param name="modifierKey">設定輔助按鍵，為 Windows MOD_XXX 系列的常數</param>
        /// <param name="key">一般按鍵</param>
        public void RegisterHotKey(Form form, int modifierKey, Keys key)
        {
            int id = (modifierKey + key).GetHashCode();
            WindowsApi.RegisterHotKey(form.Handle, id, modifierKey, (int)key);
        }

        /// <summary>
        /// 取消註冊熱鍵
        /// </summary>
        /// <param name="form">修改器本身</param>
        /// <param name="modifierKey">設定輔助按鍵，為 Windows MOD_XXX 系列的常數</param>
        /// <param name="key">一般按鍵</param>
        public void UnregisterHotKey(Form form, int modifierKey, Keys key)
        {
            int id = (modifierKey + key).GetHashCode();
            WindowsApi.UnregisterHotKey(form.Handle, id);
        }
    }
}
