﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace GameCheat
{
    internal class WindowsApi
    {
        /// <summary>
        /// 從目標程序的記憶體取值
        /// </summary>
        /// <param name="hProcess">目標程序的控制代碼 (Handle)</param>
        /// <param name="lpBaseAddress">記憶體起始位址</param>
        /// <param name="lpBuffer">用來存放取到的值的緩衝區</param>
        /// <param name="nSize">要取出的位元組數量</param>
        /// <param name="lpNumberOfBytesRead">要寫入緩衝區的實際位元組數量</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", EntryPoint = "ReadProcessMemory")]
        public static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, int nSize, IntPtr lpNumberOfBytesRead);

        /// <summary>
        /// 將值寫入目標程序的記憶體
        /// </summary>
        /// <param name="hProcess">目標程序的控制代碼 (Handle)</param>
        /// <param name="lpBaseAddress">記憶體起始位址</param>
        /// <param name="lpBuffer">用來存放要寫入的值的緩衝區</param>
        /// <param name="nSize">要寫入的值的位元組數量</param>
        /// <param name="lpNumberOfBytesWritten">要寫入記憶體的實際位元組數量</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", EntryPoint = "WriteProcessMemory")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, int nSize, IntPtr lpNumberOfBytesWritten);

        /// <summary>
        /// 開啟一個執行中的程序並回傳其控制代碼 (Handle)
        /// </summary>
        /// <param name="dwDesiredAccess">所要求的權限值，可參考 Windows.h 的 ACCESS_MASK</param>
        /// <param name="bInheritHandle">是否繼承控制代碼 (Handle)</param>
        /// <param name="dwProcessId">程序的 PID</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", EntryPoint = "OpenProcess")]
        public static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        /// <summary>
        /// 關閉並釋放控制代碼 (Handle)
        /// </summary>
        /// <param name="hObject">要關閉的控制代碼 (Handle)</param>
        [DllImport("kernel32.dll")]
        public static extern void CloseHandle(IntPtr hObject);

        /// <summary>
        /// 取得游標當前位置 (解析度 100%)
        /// </summary>
        /// <param name="lpPoint">回傳值，游標當前位置</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool GetCursorPos(out Point lpPoint);

        /// <summary>
        /// 尋找螢幕上的視窗並返回其控制代碼 (Handle)
        /// </summary>
        /// <param name="strClassName">視窗類別名稱 (可透過 Spy++ 查看)，若為 Null 則只比對標題名稱</param>
        /// <param name="strWindowName">視窗標題，若為 Null 則找所有標題</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string strClassName, string strWindowName);

        /// <summary>
        /// 尋找螢幕上的父視窗下的子視窗並返回其控制代碼 (Handle)
        /// </summary>
        /// <param name="hWndParent">父視窗的控制代碼，若為 Null 則為桌面視窗</param>
        /// <param name="hWndChildAfter">子視窗的控制代碼，若為 Null 則找第一個子視窗。須為第一層子視窗，而不能是後代視窗</param>
        /// <param name="strClassName">視窗類別名稱 (可透過 Spy++ 查看)，若為 Null 則只比對標題名稱</param>
        /// <param name="strWindowName">視窗標題，若為 Null 則找所有標題</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindowEx(IntPtr hWndParent, IntPtr hWndChildAfter, string strClassName, string strWindowName);

        /// <summary>
        /// 傳送訊息到視窗
        /// </summary>
        /// <param name="hwnd">視窗控制代碼</param>
        /// <param name="wMsg">要傳送的訊息類別，例如 WM_CHAR 等等</param>
        /// <param name="wParam">要傳送的訊息資料，值會依照 wMsg 的類別而決定，可在 MSDN 查詢</param>
        /// <param name="lParam">要傳送的訊息指標，值會依照 wMsg 的類別而決定，可在 MSDN 查詢</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);

        /// <summary>
        /// 取得視窗所在的矩形區塊 (解析度 100%)
        /// </summary>
        /// <param name="hwnd">視窗的控制代碼 (Handle)</param>
        /// <param name="lpRect">回傳值，視窗的矩形區塊資訊</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int GetWindowRect(IntPtr hwnd, out Rectangle lpRect);

        /// <summary>
        /// 讓取得視窗座標的函式其取得的座標範圍隨「當前解析度」改變
        /// </summary>
        /// <remarks>
        /// 假設螢幕解析度原始 (100%) 寬高比是 1280x800，現在調成 2560x1600 (200%)
        /// 在取得座標時無論如何都會取到原始寬高 (100%)，但呼叫此函式後會就隨當前解析度而變 (200%)
        /// </remarks>
        /// <returns></returns>
        [DllImport("user32")]
        public static extern bool SetProcessDPIAware();

        /// <summary>
        /// 註冊熱鍵，當熱鍵觸發時傳送事件訊息 WM_HOTKEY (0x0312) 到 hWnd 視窗內
        /// </summary>
        /// <param name="hWnd">要接收事件訊息的視窗的控制代碼 (Handle)</param>
        /// <param name="id">該熱鍵的 ID，可以自己取亂數，取消註冊時會用到</param>
        /// <param name="fsModifiers">設定輔助按鍵，為 Windows MOD_XXX 系列的常數</param>
        /// <param name="vk">設定一般按鍵，可透過 (int)Keys.X 設定</param>
        /// <returns>成功回傳 true，否則 false</returns>
        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);

        /// <summary>
        /// 取消註冊熱鍵
        /// </summary>
        /// <param name="hWnd">註冊此熱鍵的視窗的控制代碼 (Handle)</param>
        /// <param name="id">該熱鍵的 ID</param>
        /// <returns>成功回傳 true，否則 false</returns>
        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);
    }
}
