﻿namespace GameCheat
{
    /// <summary>
    /// 給外掛用的 Enum
    /// </summary>
    public class CheatEnum
    {
        /// <summary>
        /// 判斷程式執行狀態
        /// </summary>
        public enum ProcessState
        {
            /// <summary>
            /// 程式已關閉
            /// </summary>
            Closed,

            /// <summary>
            /// 程式執行中
            /// </summary>
            Running
        }
    }
}
