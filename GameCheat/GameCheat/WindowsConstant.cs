﻿namespace GameCheat
{
    /// <summary>
    /// Windows Api 參數會用到的常數值
    /// </summary>
    public class WindowsConstant
    {
        //註冊熱鍵時會用到的輔助按鍵
        public const int NO_MOD = 0x0000;
        public const int MOD_ALT = 0x0001;
        public const int MOD_CONTROL = 0x0002;
        public const int MOD_SHIFT = 0x0004;
        public const int MOD_WIN = 0x0008;

        //觸發熱鍵的事件 ID
        public const int WM_HOTKEY = 0x0312;
    }
}
