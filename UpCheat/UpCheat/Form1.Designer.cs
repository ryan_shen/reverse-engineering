﻿namespace UpCheat
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Layer = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ControlWithCursor = new System.Windows.Forms.CheckBox();
            this.PlayerY = new System.Windows.Forms.Label();
            this.PlayerX = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CursorY = new System.Windows.Forms.Label();
            this.CursorX = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.Gravity = new System.Windows.Forms.Label();
            this.LockGravity = new System.Windows.Forms.CheckBox();
            this.Add5Gravities = new System.Windows.Forms.Button();
            this.Subtract5Gravities = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Layer
            // 
            this.Layer.AutoSize = true;
            this.Layer.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.Layer.Location = new System.Drawing.Point(235, 265);
            this.Layer.Name = "Layer";
            this.Layer.Size = new System.Drawing.Size(25, 27);
            this.Layer.TabIndex = 23;
            this.Layer.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.label4.Location = new System.Drawing.Point(75, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 27);
            this.label4.TabIndex = 22;
            this.label4.Text = "當前階層";
            // 
            // ControlWithCursor
            // 
            this.ControlWithCursor.AutoSize = true;
            this.ControlWithCursor.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.ControlWithCursor.Location = new System.Drawing.Point(401, 193);
            this.ControlWithCursor.Name = "ControlWithCursor";
            this.ControlWithCursor.Size = new System.Drawing.Size(152, 31);
            this.ControlWithCursor.TabIndex = 21;
            this.ControlWithCursor.Text = "用滑鼠玩";
            this.ControlWithCursor.UseVisualStyleBackColor = true;
            // 
            // PlayerY
            // 
            this.PlayerY.AutoSize = true;
            this.PlayerY.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.PlayerY.Location = new System.Drawing.Point(313, 197);
            this.PlayerY.Name = "PlayerY";
            this.PlayerY.Size = new System.Drawing.Size(25, 27);
            this.PlayerY.TabIndex = 20;
            this.PlayerY.Text = "0";
            // 
            // PlayerX
            // 
            this.PlayerX.AutoSize = true;
            this.PlayerX.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.PlayerX.Location = new System.Drawing.Point(235, 197);
            this.PlayerX.Name = "PlayerX";
            this.PlayerX.Size = new System.Drawing.Size(25, 27);
            this.PlayerX.TabIndex = 19;
            this.PlayerX.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.label5.Location = new System.Drawing.Point(75, 197);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 27);
            this.label5.TabIndex = 18;
            this.label5.Text = "人物座標";
            // 
            // CursorY
            // 
            this.CursorY.AutoSize = true;
            this.CursorY.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.CursorY.Location = new System.Drawing.Point(313, 132);
            this.CursorY.Name = "CursorY";
            this.CursorY.Size = new System.Drawing.Size(25, 27);
            this.CursorY.TabIndex = 17;
            this.CursorY.Text = "0";
            // 
            // CursorX
            // 
            this.CursorX.AutoSize = true;
            this.CursorX.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.CursorX.Location = new System.Drawing.Point(235, 132);
            this.CursorX.Name = "CursorX";
            this.CursorX.Size = new System.Drawing.Size(25, 27);
            this.CursorX.TabIndex = 16;
            this.CursorX.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.label2.Location = new System.Drawing.Point(75, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 27);
            this.label2.TabIndex = 15;
            this.label2.Text = "滑鼠座標";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 30;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.label1.Location = new System.Drawing.Point(75, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 27);
            this.label1.TabIndex = 27;
            this.label1.Text = "垂直速度";
            // 
            // Gravity
            // 
            this.Gravity.AutoSize = true;
            this.Gravity.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.Gravity.Location = new System.Drawing.Point(235, 62);
            this.Gravity.Name = "Gravity";
            this.Gravity.Size = new System.Drawing.Size(25, 27);
            this.Gravity.TabIndex = 28;
            this.Gravity.Text = "0";
            // 
            // LockGravity
            // 
            this.LockGravity.AutoSize = true;
            this.LockGravity.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.LockGravity.Location = new System.Drawing.Point(329, 62);
            this.LockGravity.Name = "LockGravity";
            this.LockGravity.Size = new System.Drawing.Size(98, 31);
            this.LockGravity.TabIndex = 29;
            this.LockGravity.Text = "鎖定";
            this.LockGravity.UseVisualStyleBackColor = true;
            // 
            // Add5Gravities
            // 
            this.Add5Gravities.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.Add5Gravities.Location = new System.Drawing.Point(467, 55);
            this.Add5Gravities.Name = "Add5Gravities";
            this.Add5Gravities.Size = new System.Drawing.Size(96, 42);
            this.Add5Gravities.TabIndex = 30;
            this.Add5Gravities.Text = "+5";
            this.Add5Gravities.UseVisualStyleBackColor = true;
            this.Add5Gravities.Click += new System.EventHandler(this.Add5Gravities_Click);
            // 
            // Subtract5Gravities
            // 
            this.Subtract5Gravities.Font = new System.Drawing.Font("PMingLiU", 10F);
            this.Subtract5Gravities.Location = new System.Drawing.Point(605, 55);
            this.Subtract5Gravities.Name = "Subtract5Gravities";
            this.Subtract5Gravities.Size = new System.Drawing.Size(96, 42);
            this.Subtract5Gravities.TabIndex = 31;
            this.Subtract5Gravities.Text = "-5";
            this.Subtract5Gravities.UseVisualStyleBackColor = true;
            this.Subtract5Gravities.Click += new System.EventHandler(this.Subtract5Gravities_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Subtract5Gravities);
            this.Controls.Add(this.Add5Gravities);
            this.Controls.Add(this.LockGravity);
            this.Controls.Add(this.Gravity);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Layer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ControlWithCursor);
            this.Controls.Add(this.PlayerY);
            this.Controls.Add(this.PlayerX);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CursorY);
            this.Controls.Add(this.CursorX);
            this.Controls.Add(this.label2);
            this.Name = "Form1";
            this.Text = "UpCheat";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label Layer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox ControlWithCursor;
        private System.Windows.Forms.Label PlayerY;
        private System.Windows.Forms.Label PlayerX;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label CursorY;
        private System.Windows.Forms.Label CursorX;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Gravity;
        private System.Windows.Forms.CheckBox LockGravity;
        private System.Windows.Forms.Button Add5Gravities;
        private System.Windows.Forms.Button Subtract5Gravities;
    }
}

