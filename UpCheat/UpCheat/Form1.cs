﻿using System;
using System.Drawing;
using System.Windows.Forms;
using GameCheat;
using static GameCheat.CheatEnum;

namespace UpCheat
{
    public partial class Form1 : Form
    {
        private readonly string _gameName = "up";
        private readonly string _windowTitle = "NS-TOWER";
        private readonly CheatProcess _process;
        //程式的進入點
        private readonly int _entryAddress = 0x00400000;
        //THREADSTACK0
        private readonly int _baseAddress = 0x0019FF7C - 0x0000026C;
        //人物基址
        private int _playerAddress;
        //重力
        private int _gravity;

        public Form1()
        {
            InitializeComponent();

            _process = new CheatProcess(_gameName);
        }

        /// <summary>
        /// 遊戲執行中不斷執行的事件
        /// </summary>
        private void OnRunning()
        {
            _playerAddress = _process.ReadMemory<int>(_baseAddress);

            //顯示垂直速度
            if (LockGravity.Checked)
            {
                _process.WriteMemory(_playerAddress + 0x11A0, _gravity);
            }
            else
            {
                _gravity = _process.ReadMemory<int>(_playerAddress + 0x11A0);
            }
            Gravity.Text = _gravity.ToString();
            //顯示游標與玩家座標
            ShowCoordinate();
            //顯示當前階層
            Layer.Text = _process.ReadMemory<int>(_playerAddress + 0x131C).ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            switch (_process.State)
            {
                case ProcessState.Running:
                    OnRunning();
                    break;
                case ProcessState.Closed:
                    break;
            }
        }

        /// <summary>
        /// 顯示游標與玩家座標
        /// </summary>
        private void ShowCoordinate()
        {
            Point cursorPosition = _process.GetCursorPosition();
            int cursorX = cursorPosition.X;
            int cursorY = cursorPosition.Y;
            CursorX.Text = cursorX.ToString();
            CursorY.Text = cursorY.ToString();

            //玩家原始座標
            int playerX = _process.ReadMemory<int>(_playerAddress + 0x1194);
            int playerY = _process.ReadMemory<int>(_playerAddress + 0x1198);

            if (ControlWithCursor.Checked)
            {
                //計算玩家新座標
                var window = _process.GetWindowRectangle(_windowTitle);
                playerX = cursorX - window.Left - 60;
                if (playerX < 0)
                {
                    playerX = 0;
                }
                else if (playerX > 352)
                {
                    playerX = 352;
                }

                playerY = cursorY - window.Top - 120;
                if (playerY < 0)
                {
                    playerY = 0;
                }
                else if (playerY > 359)
                {
                    //一到 359 就算輸
                    playerY = 359;
                }

                _process.WriteMemory(_playerAddress + 0x1194, playerX);
                _process.WriteMemory(_playerAddress + 0x1198, playerY);
            }

            PlayerX.Text = playerX.ToString();
            PlayerY.Text = playerY.ToString();
        }

        /// <summary>
        /// 增加垂直速度
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Add5Gravities_Click(object sender, EventArgs e)
        {
            _gravity += 5;
        }

        /// <summary>
        /// 減少垂直速度
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Subtract5Gravities_Click(object sender, EventArgs e)
        {
            _gravity -= 5;
        }
    }
}
